<?php

use Kontentblocks\Modules\Module,
    Kontentblocks\Templating\ModuleView;


/**
 * Class ModuleImage
 */
class ModuleImage extends Module
{

    public static $defaults = array(
        'publicName' => 'Editable Image',
        'name' => 'Editable Image',
        'description' => 'editable image demo',
        'globallyAvailable' => true,
        'asTemplate' => true,
        'connect' => array('normal', 'side'),
        'id' => 'kb-image',
        'controls' => array(
            'width' => 600
        )
    );

    public function render()
    {
        $tpl = 'default.twig';

        if ($this->getEnvVar('subcontext') === 'sidebar'){
            $tpl = 'sidebar.twig';
        }

        $tpl = new ModuleView($this, $tpl);
        $tpl->setPath($this->getPath());
        return $tpl->render();

    }

    public function fields()
    {
        $this->Fields->addGroup('image', array('label' => 'Image'))
                     ->addField(
                         'image',
                         'image',
                         array(
                             'label' => 'Image',
                             'returnObj' => 'Image'
                         )
                     );
    }

}
