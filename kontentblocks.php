<?php

//add_filter('kb.remove.editor.page', '__return_false');


add_filter(
    'kb.module.paths',
    function ( $paths ) {
        $paths[] = get_stylesheet_directory() . '/modules/';
        return $paths;
    }
);

\Kontentblocks\registerArea(
    array(
        'id' => 'demo-content', // unique id of area used in do_action('area',...) call
        'name' => 'Demo Page Content', // public shown name
        'description' => 'A single demo area', // public description
        'postTypes' => array( 'page' ), // array of post types where this area is available to
        'pageTemplates' => array( 'default' ), // array of page template names where this area is available to
        'assignedModules' => array(), // array of classnames
        'layouts' => array(), // array of area template ids
        'defaultTpl' => 'default', // default Tpl to use, if none is set
        'dynamic' => false, // whether this is an dynamic area
        'manual' => true, // true if set by code
        'limit' => 0, // how many modules are allowed
        'order' => 0, // order index for sorting
        'context' => 'normal', // location on the edit screen
    )
);


\Kontentblocks\registerArea(
    array(
        'id' => 'demo-sidebar', // unique id of area used in do_action('area',...) call
        'name' => 'Demo Page Sidebar', // public shown name
        'description' => 'A single demo sidebar area', // public description
        'postTypes' => array( 'page' ), // array of post types where this area is available to
        'pageTemplates' => array( 'default' ), // array of page template names where this area is available to
        'assignedModules' => array(), // array of classnames
        'layouts' => array(), // array of area template ids
        'defaultTpl' => 'default', // default Tpl to use, if none is set
        'dynamic' => false, // whether this is an dynamic area
        'manual' => true, // true if set by code
        'limit' => 0, // how many modules are allowed
        'order' => 0, // order index for sorting
        'context' => 'side', // location on the edit screen
    )
);


/**
 * Class DemoStaticPostPanel
 */
class DemoStaticPostPanel extends \Kontentblocks\Panels\StaticPanel
{

    public function fields( \Kontentblocks\Fields\PanelFieldManager $FM )
    {
        $groupA = $FM->addGroup( 'group-a', array( 'label' => 'Testdrive' ) );
        $groupA
            ->addField(
                'image',
                'whoo',
                array(
                    'label' => 'Page Header',
                    'description' => 'Fullscreen background image',
                    'hideMeta' => true,
                    'previewSize' => array( 300, 150 ),
                    'returnObj' => 'Image'
                )
            )
            ->addField(
                'file',
                'file',
                array(
                    'label' => 'File',
                    'description' => 'Download article pdf',
                    'hideMeta' => true
                )
            );

        return $FM;
    }

}

// Register the panel on init
add_action(
    'init',
    function () {
        \Kontentblocks\Panels\PanelRegistry::getInstance()->add(
            'second',
            array(
                'baseId' => 'second',
                'postTypes' => array( 'post' ),
                'class' => 'DemoStaticPostPanel',
                'metaBox' => array('title' => 'Test panel', 'priority' => 'high')
            )
        );
    }
);


/**
 * Class DemoOptionsPanel
 */
class DemoOptionsPanel extends \Kontentblocks\Panels\OptionsPanel
{

    public function fields( \Kontentblocks\Fields\PanelFieldManager $FM )
    {
        // Store field section in variable and add fields 'later'
        $groupA = $FM->addGroup( 'group-a', array( 'label' => 'Tab #1' ) );
        $groupB = $FM->addGroup( 'group-b', array( 'label' => 'Tab #2' ) );
        $groupA
            ->addField(
                'image',
                'whoo',
                array(
                    'label' => 'Page Header',
                    'description' => 'Fullscreen background image',
                    'hideMeta' => true,
                    'previewSize' => array( 300, 150 ),
                    'returnObj' => 'Image'
                )
            );

        $groupB->addField(
            'file',
            'file',
            array(
                'label' => 'File',
                'description' => 'Download article pdf',
                'hideMeta' => true
            )
        );

        // or add Fields directly to the FieldSection
        $FM->addGroup( 'group-c', array( 'label' => 'Tab #3' ) )
           ->addField(
               'text',
               'sample',
               array(
                   'label' => 'Sample text input',
                   'description' => 'Sample description'
               )
           );

        return $FM;
    }

}

// Register the panel on init
add_action(
    'init',
    function () {

        \Kontentblocks\Panels\PanelRegistry::getInstance()->add(
            'third',
            array(
                'baseId' => 'third',
                'class' => 'DemoOptionsPanel',
                'menu' => array(
                    'name' => 'Options',
                    'slug' => 'demo-options',
                    'type' => 'menu',
                    'title' => 'Theme Options'
                )
            )
        );
    }
);