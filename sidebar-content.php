<?php
/**
 * The Content Sidebar
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

?>
<div id="content-sidebar" class="content-sidebar widget-area" role="complementary">

	<?php dynamic_sidebar( 'sidebar-2' ); ?>
    <?php do_action('sidebar_areas', null, array('context' => 'page', 'subcontext' => 'sidebar')); ?>
</div><!-- #content-sidebar -->
